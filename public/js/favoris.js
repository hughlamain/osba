

function onClickBtnFavoris(id, url) {

    const spanCount = document.getElementById('compteur_' + id);
    const icones = document.getElementById('coeur_' + id);


    fetch(url)
        .then(response => response.json())

        .then(response => {

            if (icones.classList.contains('fas')) {
                icones.classList.replace('fas', 'far');
            } else {
                icones.classList.replace('far', 'fas');
            }

            spanCount.textContent = response.favoris;
        })
    return false;
}










