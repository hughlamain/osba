<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220704075344 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_460C35ED4F335D7B343FA219E843FA89E7A2A02FFDD30F8A552837F0 ON rencontre');
        $this->addSql('CREATE FULLTEXT INDEX IDX_460C35ED4F335D7B343FA219E843FA89E7A2A02FFDD30F8A552837F06DE ON rencontre (rencontre_date, equipe_domicile, equipe_exterieure, statut_rencontre, resolution, fps, description)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE commentaire CHANGE nom nom VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE slug slug VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('DROP INDEX IDX_460C35ED4F335D7B343FA219E843FA89E7A2A02FFDD30F8A552837F06DE ON rencontre');
        $this->addSql('ALTER TABLE rencontre CHANGE nom_fichier nom_fichier VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE rencontre_date rencontre_date VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE equipe_domicile equipe_domicile VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE equipe_exterieure equipe_exterieure VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE statut_rencontre statut_rencontre VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE resolution resolution VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE fps fps VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE slug slug VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('CREATE FULLTEXT INDEX IDX_460C35ED4F335D7B343FA219E843FA89E7A2A02FFDD30F8A552837F0 ON rencontre (rencontre_date, equipe_domicile, equipe_exterieure, statut_rencontre, resolution, fps)');
        $this->addSql('ALTER TABLE user CHANGE email email VARCHAR(180) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE roles roles LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:json)\', CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE username username VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
