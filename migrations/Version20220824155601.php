<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220824155601 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rencontre ADD video VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE commentaire CHANGE nom nom VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE slug slug VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE rencontre DROP video, CHANGE nom_fichier nom_fichier VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE rencontre_date rencontre_date VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE equipe_domicile equipe_domicile VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE equipe_exterieure equipe_exterieure VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE statut_rencontre statut_rencontre VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE resolution resolution VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE fps fps VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE slug slug VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE email email VARCHAR(180) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE roles roles LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:json)\', CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE username username VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
