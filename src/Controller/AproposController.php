<?php

namespace App\Controller;

use App\Repository\RencontresRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AproposController extends AbstractController
{
    /**
     * @Route("/apropos/", name="app_apropos")
     */
    public function index(RencontresRepository $rencontreRepo, Request $request): Response
    {
        return $this->render('apropos/index.html.twig', [
            'favoris' => $rencontreRepo->findAll(),
        ]);
    }
}
