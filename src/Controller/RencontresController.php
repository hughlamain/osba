<?php

namespace App\Controller;

use App\Entity\RencontreFavori;
use App\Entity\Rencontres;
use App\Entity\User;
use App\Form\EnvoiSelectionType;
use App\Repository\RencontreFavoriRepository;
use App\Repository\RencontresRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


class RencontresController extends AbstractController
{
    /**
     * @Route("/rencontres/", name="mesfavoris")
     */
    public function index(RencontresRepository $rencontreRepo, Request $request): Response
    {
        $form = $this->createForm(EnvoiSelectionType::class);

        $form->handleRequest($request);

        //tableau des favoris de l'utilisateur courant
        return $this->render(
            'rencontres/index.html.twig',
            [
                'favoris' => $rencontreRepo->findAll(),
                'form' => $form->createView()
            ]
        );
    }


    /**
     * Permet de mettre en favori ou retirer un favori
     *
     * @Route("/rencontres/{id}/favoris", name="favoris")
     * 
     * @param \App\Entity\Rencontres $rencontres
     * @param \Doctrine\ORM\EntityManagerInterface $manager
     * @param \App\Repository\RencontresRepository $rencontresRepo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function favoris(Rencontres $rencontres, RencontreFavoriRepository $rencontreFavoriRepo, EntityManagerInterface $manager): Response
    {
        $user = $this->getUser();

        if (!$user) return $this->json([
            'code' => 403,
            'message' => "pas autorisé"
        ], 403);

        if ($rencontres->isFavorisByUser($user)) {
            $favoris = $rencontreFavoriRepo->findOneBy([
                'rencontre' => $rencontres,
                'user' => $user
            ]);

            $manager->remove($favoris);
            $manager->flush();

            return $this->json([
                'code' => 200,
                'message' => 'Favori bien supprimé...',
                'favoris' => $rencontreFavoriRepo->count(['rencontre' => $rencontres])
            ], 200);
        }

        $favoris = new RencontreFavori();
        $favoris->setRencontre($rencontres)
            ->setUser($user);

        $manager->persist($favoris);
        $manager->flush();

        return $this->json([
            'code' => 200,
            'message' => 'Favori bien ajouté !',
            'favoris' => $rencontreFavoriRepo->count(['rencontre' => $rencontres])

        ], 200);
    }

    /**
     * Supprime favori dans la liste des favoris du user
     * @Route("/rencontres/{id}/delete", name="favori_delete")
     *
     * @param Rencontres $rencontres
     * @param RencontreFavoriRepository $rencontreFavoriRepo
     * @param EntityManagerInterface $manager
     * @return RedirectResponse
     */
    public function delete(
        Rencontres $rencontres,
        RencontreFavoriRepository $rencontreFavoriRepo,
        EntityManagerInterface $manager
    ): RedirectResponse {
        $user = $this->getUser();

        if ($rencontres->isFavorisByUser($user)) {
            $favoris = $rencontreFavoriRepo->findOneBy([
                'rencontre' => $rencontres,
                'user' => $user
            ]);

            $manager->remove($favoris);
            $manager->flush();

            return $this->redirectToRoute("mesfavoris");
        }
    }
}
