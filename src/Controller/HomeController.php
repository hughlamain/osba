<?php

namespace App\Controller;

use App\Form\SearchRencontreType;
use App\Repository\RencontresRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(RencontresRepository $rencontresRepository, Request $request)
    {
        $toutesrencontres = $rencontresRepository->findAll(
            ['rencontreDate' => 'desc']
        );

        $rencontres = $rencontresRepository->findBy(
            ['rencontreDate' => 'desc'],
        );

        $form = $this->createForm(SearchRencontreType::class);

        $search = $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //Recherche les fichiers correspondants aux mots clés
            $rencontres = $rencontresRepository->search($search->get('mots')
                ->getData());
        }

        return $this->render('home/index.html.twig', [
            'toutesrencontres' => $toutesrencontres,
            'rencontres' => $rencontres,
            'form' => $form->createView()
        ]);
    }
}
