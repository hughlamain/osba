<?php

namespace App\Controller\Admin;

use App\Entity\Rencontres;

use App\Form\RencontresType;

use App\Repository\RencontresRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/admin", name="admin_")
 * 
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(RencontresRepository $rencontreRepo): Response
    {
        return $this->render(
            'admin/index.html.twig',
            [
                'rencontres' => $rencontreRepo->findAll(),
            ]
        );
    }


    /**
     * @Route("/rencontres/ajout", name="rencontres_ajout")
     */
    public function ajoutRencontre(Request $request): Response
    {
        $rencontre = new Rencontres;

        $form = $this->createForm(RencontresType::class, $rencontre);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($rencontre);
            $em->flush();

            return $this->redirectToRoute('admin_home');
        }

        return $this->render('admin/rencontres/ajoutrencontre.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/modifierRencontre/{id}", name="modifier_rencontre")
     */
    public function modifierRencontre(Rencontres $rencontre, Request $request): Response
    {

        $form = $this->createForm(RencontresType::class, $rencontre);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($rencontre);
            $em->flush();

            $this->addFlash('message', 'Le match a bien été modifié');
            return $this->redirectToRoute('admin_home');
        }

        return $this->render('admin/rencontres/ajoutrencontre.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/supprimerRencontre/{id}", name="supprimer_rencontre")
     */
    public function supprimerRencontre(Rencontres $rencontre, EntityManagerInterface $entityManager)
    {

        $entityManager->remove($rencontre);
        $entityManager->flush();

        $this->addFlash('message', 'Le match a bien été supprimé');
        return $this->redirectToRoute('admin_home');
    }
}
