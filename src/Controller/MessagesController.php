<?php

namespace App\Controller;

use App\Entity\Messages;
use App\Form\MessagesType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;


class MessagesController extends AbstractController
{
    /**
     * @Route("/messages", name="messages")
     */
    public function index(): Response
    {
        return $this->render('messages/index.html.twig', [
            'controller_name' => 'MessagesController',
        ]);
    }

    /**
     * @Route("/send", name="send")
     *
     */
    public function send(Request $request, EntityManagerInterface $entityManager, MailerInterface $mailer): Response
    {
        $message = new Messages;

        $form = $this->createForm(MessagesType::class, $message);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isSubmitted()) {
            $message->setSender($this->getUser());

            $entityManager->persist($message);
            $entityManager->flush();

            //Envoi d'email
            $email = (new TemplatedEmail())
                ->from($message->getSender()->getEmail())
                ->to('osba@gmail.com')
                ->subject($message->getTitle())
                ->text($message->getMessage())
                ->htmlTemplate('emails/message.html.twig')

                // pass variables (name => value) to the template
                ->context([

                    "message" => $message
                ]);

            $mailer->send($email);

            $this->addFlash("message", "Message envoyé avec succés");
            return $this->redirectToRoute("messages");
        }

        return $this->render("messages/send.html.twig", [
            "form" => $form->createView()

        ]);
    }

    /**
     * @Route("/received", name="received")
     */
    public function received(): Response
    {
        return $this->render('messages/received.html.twig');
    }

    /**
     * @Route("/read/{id}", name="read")
     */
    public function read(Messages $message, EntityManagerInterface $entityManager): Response
    {
        $message->setIsRead(true);

        $entityManager->persist($message);
        $entityManager->flush();

        return $this->render('messages/read.html.twig', compact("message"));
    }
}
