<?php

namespace App\Repository;

use App\Entity\Rencontres;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Rencontres|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rencontres|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rencontres[]    findAll()
 * @method Rencontres[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RencontresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rencontres::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Rencontres $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Rencontres $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * Recherche les rencontres en fonction du formulaire
     * @return void
     */
    public function search($mots)
    {
        $query = $this->createQueryBuilder('r');
        if ($mots != null) {
            $query->where('MATCH_AGAINST(r.rencontreDate, r.equipeDomicile, r.equipeExterieure, r.statutRencontre,
            r.resolution, r.fps, r.description) AGAINST (:mots boolean)>0')
                ->setParameter('mots', $mots);
        }

        return $query->getQuery()->getResult();
    }

    // /**
    //  * @return Rencontres[] Returns an array of Rencontres objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Rencontres
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
