<?php

namespace App\Repository;

use App\Entity\RencontreFavori;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RencontreFavori|null find($id, $lockMode = null, $lockVersion = null)
 * @method RencontreFavori|null findOneBy(array $criteria, array $orderBy = null)
 * @method RencontreFavori[]    findAll()
 * @method RencontreFavori[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RencontreFavoriRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RencontreFavori::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(RencontreFavori $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(RencontreFavori $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return RencontreFavori[] Returns an array of RencontreFavori objects
    //  */

    // public function findAllUserFavoris($favoris): array
    // {
    //     $qb = $this->createQueryBuilder('f')
    //             ->where('user_id = app_user_id')



    //     return $query->getResult();
    // }


    /*
    public function findOneBySomeField($value): ?RencontreFavori
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
