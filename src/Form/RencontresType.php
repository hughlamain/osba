<?php

namespace App\Form;

use App\Entity\Rencontres;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RencontresType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom_fichier', TextType::class)
            ->add('photo_home', TextType::class)
            ->add('photo_versus', TextType::class)
            ->add('video', TextType::class)
            ->add('rencontreDate', TextType::class)
            ->add('equipeDomicile', TextType::class)
            ->add('equipeExterieure', TextType::class)
            ->add('resolution', TextType::class)
            ->add('fps', TextType::class)
            ->add('description', TextType::class)
            ->add('slug', TextType::class)
            ->add('statut_rencontre', TextType::class)
            ->add('valider', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Rencontres::class,
        ]);
    }
}
