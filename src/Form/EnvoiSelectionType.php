<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\RencontreFavori;

class EnvoiSelectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'disabled' => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'Votre e-mail',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('rencontre', TextType::class, [
                'disabled' => true,
                'attr' => [
                    'class' => 'form-control'
                ],
                'data' => 'test',
            ])
            ->add('message', CKEditorType::class, [
                'label' => 'Votre message',
                'data' => 'Coller ici les ID des rencontres à envoyer à OSBA'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RencontreFavori::class,
        ]);
    }
}
