<?php

namespace App\Entity;

use App\Repository\RencontresRepository;
use App\Entity\User;
use App\Entity\RencontreFavori;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RencontresRepository::class)
 * @ORM\Table(name="rencontre", indexes={@ORM\Index(columns={"rencontre_date", "equipe_domicile", "equipe_exterieure", "statut_rencontre",
 *  "resolution", "fps", "description"}, flags={"fulltext"})})
 */
class Rencontres
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_fichier;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rencontreDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $equipeDomicile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $equipeExterieure;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $statutRencontre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $resolution;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fps;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;


    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="favoris")
     */
    private $favoris;

    /**
     * @ORM\OneToMany(targetEntity=RencontreFavori::class, mappedBy="rencontre", orphanRemoval=true)
     */
    private $rencontreFavoris;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $video;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photohome;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photoversus;

    public function __construct()
    {
        $this->favoris = new ArrayCollection();
        $this->rencontreFavoris = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomFichier(): ?string
    {
        return $this->nom_fichier;
    }

    public function setNomFichier(string $nom_fichier): self
    {
        $this->nom_fichier = $nom_fichier;

        return $this;
    }

    public function getRencontreDate(): ?string
    {
        return $this->rencontreDate;
    }

    public function setRencontreDate(string $rencontreDate): self
    {
        $this->rencontreDate = $rencontreDate;

        return $this;
    }

    public function getEquipeDomicile(): ?string
    {
        return $this->equipeDomicile;
    }

    public function setEquipeDomicile(string $equipeDomicile): self
    {
        $this->equipeDomicile = $equipeDomicile;

        return $this;
    }

    public function getEquipeExterieure(): ?string
    {
        return $this->equipeExterieure;
    }

    public function setEquipeExterieure(string $equipeExterieure): self
    {
        $this->equipeExterieure = $equipeExterieure;

        return $this;
    }

    public function getStatutRencontre(): ?string
    {
        return $this->statutRencontre;
    }

    public function setStatutRencontre(string $statutRencontre): self
    {
        $this->statutRencontre = $statutRencontre;

        return $this;
    }

    public function getResolution(): ?string
    {
        return $this->resolution;
    }

    public function setResolution(string $resolution): self
    {
        $this->resolution = $resolution;

        return $this;
    }

    public function getFps(): ?string
    {
        return $this->fps;
    }

    public function setFps(string $fps): self
    {
        $this->fps = $fps;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getFavoris(): Collection
    {
        return $this->favoris;
    }

    public function addFavori(User $favori): self
    {
        if (!$this->favoris->contains($favori)) {
            $this->favoris[] = $favori;
        }

        return $this;
    }

    public function removeFavori(User $favori): self
    {
        $this->favoris->removeElement($favori);

        return $this;
    }

    /**
     * Savoir si une rencontre est mis en favoris par l'utilisateur
     *
     * @param User $user
     * @return boolean
     */
    public function isFavorisByUser(User $user): bool
    {
        foreach ($this->rencontreFavoris as $favori) {
            if ($favori->getUser() === $user) return true;
        }

        return false;
    }

    /**
     * @return Collection<int, RencontreFavori>
     */
    public function getRencontreFavoris(): Collection
    {
        return $this->rencontreFavoris;
    }

    public function addRencontreFavori(RencontreFavori $rencontreFavori): self
    {
        if (!$this->rencontreFavoris->contains($rencontreFavori)) {
            $this->rencontreFavoris[] = $rencontreFavori;
            $rencontreFavori->setRencontre($this);
        }

        return $this;
    }

    public function removeRencontreFavori(RencontreFavori $rencontreFavori): self
    {
        if ($this->rencontreFavoris->removeElement($rencontreFavori)) {
            // set the owning side to null (unless already changed)
            if ($rencontreFavori->getRencontre() === $this) {
                $rencontreFavori->setRencontre(null);
            }
        }

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(?string $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getPhotohome(): ?string
    {
        return $this->photohome;
    }

    public function setPhotohome(?string $photohome): self
    {
        $this->photohome = $photohome;

        return $this;
    }

    public function getPhotoversus(): ?string
    {
        return $this->photoversus;
    }

    public function setPhotoversus(?string $photoversus): self
    {
        $this->photoversus = $photoversus;

        return $this;
    }
}
