<?php

namespace App\Entity;

use App\Repository\RencontreFavoriRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RencontreFavoriRepository::class)
 */
class RencontreFavori
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Rencontres::class, inversedBy="rencontreFavoris")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rencontre;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="rencontreFavoris")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRencontre(): ?Rencontres
    {
        return $this->rencontre;
    }

    public function setRencontre(?Rencontres $rencontre): self
    {
        $this->rencontre = $rencontre;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
